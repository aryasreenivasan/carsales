//
//  ApiManager.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

class ApiManager: NSObject {
    let HEADER  = "https://app-car.carsalesnetwork.com.au"
    let LISTING = "stock/car/test/v1/listing"
    let AUTH    = "?username=test&password=2h7H53eXsQupXvkz"
    
    func getListingResponseFromAPI(completion: @escaping (ListResponse, _ error:Error?) -> ()) {
        
        let requestURLString = "\(HEADER)/\(LISTING)\(AUTH)"
        let request = NSMutableURLRequest(url: NSURL(string: requestURLString)! as URL)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                completion(ListResponse([:]),error)
            } else {
                do {
                    if  let jsonDict = try JSONSerialization.jsonObject(with: data!) as? [String:Any] {
                        let listResponse = ListResponse(jsonDict)
                        completion(listResponse,nil)
                    }else {
                        completion(ListResponse([:]),nil)
                    }
                } catch let parsingError {
                    completion(ListResponse([:]),parsingError)
                }
            }
        })
        dataTask.resume()
    }
    
    func getDetailResponseFromAPI(detailsUrl:String, completion: @escaping (DetalResponse, _ error:Error?) -> ()) {
        
        let requestURLString = "\(HEADER)/\(detailsUrl)\(AUTH)"
        let request = NSMutableURLRequest(url: NSURL(string: requestURLString)! as URL)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                completion(DetalResponse([:]),error)
            } else {
                do {
                    if  let jsonDict = try JSONSerialization.jsonObject(with: data!) as? [String:Any] {
                        let listResponse = DetalResponse(jsonDict)
                        completion(listResponse,nil)
                    }else {
                        completion(DetalResponse([:]),nil)
                    }
                } catch let parsingError {
                    completion(DetalResponse([:]),parsingError)
                }
            }
        })
        dataTask.resume()
    }
}
