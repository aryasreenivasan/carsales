//
//  ExtensionManager.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

extension UIImageView{
    public var imageHeightForProject: CGFloat {
        var imageWidth = UIScreen.main.bounds.size.width
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            imageWidth = UIScreen.main.bounds.size.width/2.0
            if (UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft ||
                UIDevice.current.orientation == UIDeviceOrientation.landscapeRight) {
                imageWidth = UIScreen.main.bounds.size.width/3.0
            }
        }else {
            if (UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft ||
                UIDevice.current.orientation == UIDeviceOrientation.landscapeRight) {
                imageWidth = UIScreen.main.bounds.size.width/2.0
            }
        }
        return (imageWidth*(2.0/3.0))
    }
}

extension UILabel{
    
    public var requiredHeight: CGFloat {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text
        label.attributedText = attributedText
        label.sizeToFit()
        return label.frame.height
    }
}

extension UIDeviceOrientation {
    static var isLandscape: Bool {
        if (UIDevice.current.orientation == UIDeviceOrientation.landscapeLeft ||
            UIDevice.current.orientation == UIDeviceOrientation.landscapeRight) {
            return true
        }
        return false
    }
}


extension UIViewController {
    func displaySingleButtonAlert(message:String?) {
        DispatchQueue.main.async {
            let messageString = message ?? "Network error occurred"
            let alert = UIAlertController(title: "", message: messageString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

extension UIDevice {
    static var isiPad: Bool {
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad) {
            return true
        }
        return false
    }
}

extension UICollectionView {
    func setCustomFlowLayout() {
        if let flowLayout = self.collectionViewLayout as? UICollectionViewFlowLayout {
            var width = UIScreen.main.bounds.size.width-20
            if (UIDevice.isiPad) {
                width = (UIScreen.main.bounds.size.width/2.0)-20
                if (UIDeviceOrientation.isLandscape) {
                    width = (UIScreen.main.bounds.size.width/3.0)-20
                }
            }else {
                if (UIDeviceOrientation.isLandscape) {
                    width = (UIScreen.main.bounds.size.width/2.0)-20
                }
            }
            flowLayout.estimatedItemSize = CGSize(width: width, height: 200)
        }
    }
}
