//
//  ListResponse.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

class ListResponse: NSObject {
    var result:[ListResult] = []
    init(_ dictionary: [String:Any]) {
        self.result = []
        let resultsArray = dictionary["Result"] as? [[String:Any]] ?? []
        for i in 0..<resultsArray.count {
            let objectAtIndex = ListResult(resultsArray[i])
            self.result.append(objectAtIndex)
        }
    }
}

class ListResult: NSObject {
    var id:String           = ""
    var title:String        = ""
    var location:String     = ""
    var price:String        = ""
    var mainPhoto:String    = ""
    var detailsUrl:String   = ""
    
    init(_ dictionary: [String:Any]) {
        self.id         = dictionary["Id"] as? String ?? ""
        self.title      = dictionary["Title"] as? String ?? ""
        self.location   = dictionary["Location"] as? String ?? ""
        self.price      = dictionary["Price"] as? String ?? ""
        self.mainPhoto  = dictionary["MainPhoto"] as? String ?? ""
        self.detailsUrl = dictionary["DetailsUrl"] as? String ?? ""
        
    }
}
