//
//  DetalResponse.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

class DetalResponse: NSObject {
    var result:[DetalResult] = []
    
    init(_ dictionary: [String:Any]) {
        self.result = []
        let resultsArray = dictionary["Result"] as? [[String:Any]] ?? []
        for i in 0..<resultsArray.count {
            let objectAtIndex = DetalResult(resultsArray[i])
            self.result.append(objectAtIndex)
        }
    }
}

class DetalResult: NSObject {
    var id:String           = ""
    var saleStatus:String   = ""
    var overview:OverView   = OverView([:])
    var comments:String     = ""
    
    init(_ dictionary: [String:Any]) {
        self.id         = dictionary["Id"] as? String ?? ""
        self.saleStatus = dictionary["SaleStatus"] as? String ?? ""
        self.overview   = OverView(dictionary["Overview"] as? [String:Any] ?? [:])
        self.comments   = dictionary["Comments"] as? String ?? ""
        
    }
}

class OverView: NSObject {
    var location:String = ""
    var price:String    = ""
    var photos:[String] = []
    
    init(_ dictionary: [String: Any]) {
        self.location   = dictionary["Location"] as? String ?? ""
        self.price      = dictionary["Price"] as? String ?? ""
        self.photos     = dictionary["Photos"] as? [String] ?? []
    }
}
