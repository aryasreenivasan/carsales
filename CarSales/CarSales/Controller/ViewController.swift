//
//  ViewController.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    //MARK: Outlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- Variables
    var dataArray:[ListResult] = []
    var refresher:UIRefreshControl!
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresher = UIRefreshControl()
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self, action: #selector(fetchResponseFromAPI), for: .valueChanged)
        collectionView!.addSubview(refresher)
    }
    
    override func viewWillLayoutSubviews() {
        collectionView.setCustomFlowLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchResponseFromAPI()
    }
    
    //MARK:- Fetch Data From APIManager
    @objc func fetchResponseFromAPI() {
        SVProgressHUD.show()
        ApiManager().getListingResponseFromAPI { (responseDict, error) in
            if (error == nil) {
                self.dataArray = responseDict.result 
            }else {
                self.displaySingleButtonAlert(message: error?.localizedDescription)
            }
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.refresher.endRefreshing()
                self.collectionView.reloadData()
            }
        }
    }
    
    //MARK:- Collection View Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArray.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ListCollectionViewCell", for: indexPath) as! ListCollectionViewCell
        cell.configureCell(listResult: dataArray[indexPath.row])
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextVC = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        nextVC.listResult = dataArray[indexPath.row]
        self.navigationController?.pushViewController(nextVC, animated: true)
    } 
    
}

