//
//  DetailViewController.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UITableViewDataSource {

    //MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Variables
    var listResult:ListResult = ListResult([:])
    var detailResult:[DetalResult] = []
    var refresher:UIRefreshControl!
    
    //MARK:- View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refresher = UIRefreshControl()
        refresher.tintColor = UIColor.gray
        refresher.addTarget(self, action: #selector(fetchResponseFromAPI), for: .valueChanged)
        tableView!.addSubview(refresher)
    }
    
    override func viewWillLayoutSubviews() {
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleLabel.text = listResult.title 
        fetchResponseFromAPI()
    }
    
    @objc func fetchResponseFromAPI() {
        SVProgressHUD.show()
        ApiManager().getDetailResponseFromAPI(detailsUrl: listResult.detailsUrl) { (responseDict, error) in
            if (error == nil) {
                self.detailResult = responseDict.result
            }else {
                self.displaySingleButtonAlert(message: error?.localizedDescription)
            }
            DispatchQueue.main.async { 
                SVProgressHUD.dismiss()
                self.refresher.endRefreshing()
                self.tableView.reloadData()
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTableViewCell", for: indexPath) as! DetailTableViewCell
        if (self.detailResult.count>indexPath.row) {
            cell.configureCell(detailResult: self.detailResult[0])
        }
        return cell
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
