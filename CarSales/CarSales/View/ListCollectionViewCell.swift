//
//  ListCollectionViewCell.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

class ListCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var bannerView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.backgroundColor = UIColor.clear
        self.priceLabel.backgroundColor = UIColor.clear
        self.locationLabel.backgroundColor = UIColor.clear
        self.titleLabel.text = ""
        self.priceLabel.text = ""
        self.locationLabel.text = "" 
    }
    
    func configureCell(listResult:ListResult) {
        self.titleLabel.text = listResult.title
        self.priceLabel.text = listResult.price
        self.locationLabel.text = listResult.location
        
        var imageUrlString:String = listResult.mainPhoto
        imageUrlString = imageUrlString.replacingOccurrences(of: " ", with: "%20")
        let imageUrl = URL(string: imageUrlString)
        JMImageCache.shared()?.image(for: imageUrl, completionBlock: { (image) in
            self.bannerView.image = image
        }, failureBlock: { (request, response, error) in
            
        })
    }
    
    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
        setNeedsLayout()
        layoutIfNeeded()
        let totalHeight = bannerView.imageHeightForProject+titleLabel.requiredHeight+priceLabel.requiredHeight+locationLabel.requiredHeight+30
        layoutAttributes.frame = CGRect(x: 0, y: 0, width: layoutAttributes.frame.size.width, height: totalHeight)
        return layoutAttributes
    }
}
