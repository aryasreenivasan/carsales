//
//  DetailTableViewCell.swift
//  CarSales
//
//  Created by Arya S on 13/07/19.
//  Copyright © 2019 AryaSreenivasan. All rights reserved.
//

import UIKit

class DetailTableViewCell: UITableViewCell {

    @IBOutlet weak var topScrollView: UIScrollView!
    @IBOutlet weak var scrollBase: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var salesStatusLabel: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.scrollBase.backgroundColor = UIColor.clear
        self.locationLabel.backgroundColor = UIColor.clear
        self.priceLabel.backgroundColor = UIColor.clear
        self.salesStatusLabel.backgroundColor = UIColor.clear
        self.commentsLabel.backgroundColor = UIColor.clear
        
        self.locationLabel.text = ""
        self.priceLabel.text = ""
        self.salesStatusLabel.text = ""
        self.commentsLabel.text = ""
    }
    
    func configureCell(detailResult:DetalResult) {
        self.locationLabel.text = detailResult.overview.location
        self.priceLabel.text = detailResult.overview.price
        self.salesStatusLabel.text = detailResult.saleStatus
        self.commentsLabel.text = detailResult.comments
        
        let screenWidth:CGFloat = UIScreen.main.bounds.size.width
        let imageHeight:CGFloat = UIScreen.main.bounds.size.width*(2.0/3.0)
        if (detailResult.overview.photos.count>0) {
            for i in 0..<detailResult.overview.photos.count {
                let banner = UIImageView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: imageHeight))
                banner.center = CGPoint(x: (UIScreen.main.bounds.size.width/2.0)+(UIScreen.main.bounds.size.width*CGFloat(i)), y: banner.center.y)
                banner.clipsToBounds = true
                banner.backgroundColor = UIColor.clear
                banner.contentMode = UIView.ContentMode.scaleAspectFill
                self.topScrollView.addSubview(banner)
                
                var imageUrlString = detailResult.overview.photos[i]
                imageUrlString = imageUrlString.replacingOccurrences(of: " ", with: "%20")
                let imageUrl = URL(string: imageUrlString)
                JMImageCache.shared()?.image(for: imageUrl, completionBlock: { (image) in
                    banner.image = image
                }, failureBlock: { (request, response, error) in
                    
                })
            }
            self.topScrollView.contentSize = CGSize(width: (screenWidth*CGFloat(detailResult.overview.photos.count)), height: 0)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated) 
    }

}
