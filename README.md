# Carsales

## Project loads data from Carsales REST API
### It contains a page with the list of cars, and the detail page with it's details 

## Achieved tasks
===============================================================
### Listing Cell:
* Photo must be in 3:2 ratio, and 100% of the cell width
* Title and Price to be left aligned
* Anchor the Location to the bottom-right corner of the cell
* The cell should scale correctly to different screen sizes
* Tapping on the cell should display the Detail Screen
* Bonus: show a grid of cells on iPad with 2 columns in portrait and 3 columns in landscape

### Detail Screen:
* Photo must be in 3:2 ratio, and 100% of the screen width
* Location, Price, Sales Status and Comments to be left aligned
* Comments should support any number of lines
* The Detail Screen should scale correctly to different screen sizes
* Bonus: show multiple photos scrollable from left to right

### Source control/Git repository
* Used BitBucket

#### Approach & Time taken
* Task was interesting - Especially the collection view layout adjustment & dynamic size in all devices and in all orientations.
* Started the project around 01:00 am local time. Finished around 03:15 am (Total time taken: 02:15 hours)
* Completed the tasks as per the following time frame:
    * Initial project commit:  Created new XCode Project around - 01:08
    * Created Model class for API Calls, Fetched data from listing url - 01:23
    * Implemented List Response Model. Altered ApiManager for the new model - 01:35
    * Implemented collection view on home page. Adjusted cell hight according to content - 02:02
    * Implemented detal page - 02:50
    * Completed the project tasks + all Bonus tasks (in Portrait & Landscape) - 03:13
* Methods followed:
    * Used MVC Architecture
    * Committed the project after each tasks
    * Creating detailed Readme.md for better understanding

#### All Tasks completed successfully

## Additional accomplishments
===============================================================  
✅ Image cache added - with the help oj JMImageCache Library  
✅ Improved user experiance by SVProgressHUD loader  
✅ Improved user experiance by integrating pull to refresh feature in both pages of the application  
✅ Downloaded company logo from PlayStore and designed the LaunchScreen with the logo & placed the same logo in cars listing page title bar  
✅ Used the color theme in the app to match the company standards  
